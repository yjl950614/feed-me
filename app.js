"use strict";

var express = require("express");
var timeout = require("connect-timeout");
var AV = require("leanengine");

// var child_process = require('child_process');
var app = express();
// 设置默认超时时间
app.use(timeout("15s"));
// 加载云引擎中间件
app.use(AV.express());
app.enable("trust proxy");
// 需要重定向到 HTTPS
app.use(AV.Cloud.HttpsRedirect());

app.use('/', require('./routes/main'));

module.exports = app;
